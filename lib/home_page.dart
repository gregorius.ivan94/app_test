import 'package:flutter/material.dart';

/// This is the main application widget.
class HomePage extends StatelessWidget {
  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: MyStatefulWidget(),
    );
  }
}

/// This is the stateful widget that the main application instantiates.
class MyStatefulWidget extends StatefulWidget {
  MyStatefulWidget({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 0;

  List<Widget> _widgetOptions = <Widget>[
    ListView(
      children: const <Widget>[
        Card(
          color: Color(0xfff3a6b8),
          child: ListTile(
            leading: FlutterLogo(size: 56.0),
            title: Text('Boombayah', style: TextStyle(color: Colors.white)),
            subtitle: Text('Here is a second line',
                style: TextStyle(color: Colors.white)),
            trailing: Icon(Icons.more_vert),
          ),
        ),
        Card(
          color: Color(0xfff3a6b8),
          child: ListTile(
            leading: FlutterLogo(size: 56.0),
            title: Text('Ice cream', style: TextStyle(color: Colors.white)),
            subtitle: Text('Here is a second line',
                style: TextStyle(color: Colors.white)),
            trailing: Icon(Icons.more_vert),
          ),
        ),
        Card(
          color: Color(0xfff3a6b8),
          child: ListTile(
            leading: FlutterLogo(size: 56.0),
            title: Text('Forever young', style: TextStyle(color: Colors.white)),
            subtitle: Text('Here is a second line',
                style: TextStyle(color: Colors.white)),
            trailing: Icon(Icons.more_vert),
          ),
        ),
      ],
    ),
    Icon(Icons.directions_transit),
    Icon(Icons.directions_bike),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        title:
            const Text('BLACKPINK', style: TextStyle(color: Color(0xfff3a6b8))),
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.black,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.music_note),
            title: Text('Song'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text('Profile'),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Color(0xfff3a6b8),
        unselectedItemColor: Colors.white,
        onTap: _onItemTapped,
      ),
    );
  }
}
